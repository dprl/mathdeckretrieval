from flask import Flask, request, jsonify
from flask_cors import cross_origin
from MathEntityCard import MathEntityCard

import time
import sys
import logging
import logging.handlers
import os

#Init App
app = Flask(__name__)

#Init Class
mec = MathEntityCard()

@app.route('/entityCards', methods=['POST'])
@cross_origin()
def get_entity_card_details():
    req_data = request.get_json()
    start = time.time()
    latex_key = 'formula'
    formula_latex = req_data[latex_key] if (latex_key in req_data) else None

    mml_key = 'formulaMathML'
    formula_mml = req_data[mml_key] if (mml_key in req_data) else None
    print("----------------------")
    print(formula_mml)
    print(formula_latex)
    print("----------------------")
    if formula_mml and formula_mml != '':
        print("we are here!")
        result = mec.get_entity_card_mml(formula_mml)
    elif formula_latex:
        result = mec.get_entity_card(f'${formula_latex}$')
    else:
        result = ''
    print(' Total time',(time.time()-start))
    return result


@app.route('/entityCardsName', methods=['POST'])
@cross_origin()
def get_entity_card_by_name():
    req_data = request.get_json()
    # start = time.time()
    text_key = 'textQuery'
    text_query = req_data[text_key] if (text_key in req_data) else None
    lower_text_query = text_query.lower()
    if text_query and text_query != '':
        result = mec.get_entity_card_title_alias(lower_text_query)
    else:
        result = ''
    # print(' Total time For text based query',(time.time()-start))
    return result

if __name__ == '__main__':
    app.run(debug=False, port="5010")
