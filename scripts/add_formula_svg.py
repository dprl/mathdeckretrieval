import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
import json

from MathEntityCard import MathEntityCard

mec = MathEntityCard()
def main():


    with open('formulas.json') as json_file:
        all_formulas = json.load(json_file)

    conn = mec.conn

    curr = conn.cursor()
    for formula_data in all_formulas:

        formula_id_key = 'id'
        formula_id = formula_data[formula_id_key] if (formula_id_key in formula_data) else None

        mml_key = 'mml'
        formula_mml = formula_data[mml_key] if (mml_key in formula_data) else None

        symbols_key = 'symbols'
        symbols_json = formula_data[symbols_key] if (symbols_key in formula_data) else None

        if formula_mml and symbols_json:
            curr.execute('UPDATE formulas '
            ' SET formula_mathml=:mml, formula_symbols_json =:symbols '
            ' WHERE  formula_id =:id',{'id':formula_id,
            'mml':str(formula_mml),'symbols':json.dumps(symbols_json)})
    conn.commit()
    
if __name__ == "__main__":
    main()