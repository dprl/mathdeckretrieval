import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from MathEntityCard import MathEntityCard, EntityCard, Description, Formula
from formula_embedding_library import MathExtractor as ME
import traceback

""" 
    In case of change in Embedding Model (Behrooz)
    Run this script to  update all embedding in existing Database.
"""
__author__ ='Abishai Dmello'


def main():
    mec = MathEntityCard()

    curr = mec.conn.cursor()

    curr.execute(" Select formula_id, formula_latex from formulas ")
    all_rows = curr.fetchall()
    for row  in all_rows:
        formula_id = row[0]
        print(formula_id)
        formula_latex = row[1]
        try : 
            formula_mml = ME.pandoc_tex_mml(f'${formula_latex}$')
            formula_embedding = mec.get_latex_embedding(formula_mml)
        
            formula_embedding_blob = formula_embedding.tostring()

            sql = " UPDATE formulas set formula_embed = :formula_embedding_blob where formula_id = :formula_id"
            data = {'formula_embedding_blob':formula_embedding_blob,'formula_id':formula_id}
            curr.execute(sql,data)
            mec.conn.commit()
        except Exception :
            mec.conn.rollback()
            tb = traceback.format_exc()
            print(tb)



if __name__ == '__main__':
    main()