import time

import numpy
import torch
from collections import Counter
from .tangent_s import Tuple_Extraction
from .embedding_preprocessing.tuple_encoder import TupleTokenizationMode
from .math_deck_encoder import MathDeckEncoder
from .tangent_cft_module import TangentCFTModule


class FormulaEmbedding:
    def __init__(self, slt_model_file_path, opt_model_file_path, slt_type_model_file_path):
        """
        Formula embedding library is based on Tangent-CFT model, it take the saved slt, opt and slt-type embedding
        models and loads them. It can also take in a collection of formulas in form of a dictionary of (formula id,
        vector [numpy])
        :param slt_model_file_path: file path for the saved slt model
        :param opt_model_file_path: file path for the saved opt model
        :param slt_type_model_file_path: file path for the saved slt-type model
        """
        self.math_encoder = MathDeckEncoder()
        self.math_encoder.load_encoder_map("formula_embedding_library/embedding_preprocessing/encoder_math_deck.csv")
        self.slt_model = TangentCFTModule(slt_model_file_path)
        self.opt_model = TangentCFTModule(opt_model_file_path)
        self.slt_type_model = TangentCFTModule(slt_type_model_file_path)

        self.__set_collection()

    def __set_collection(self, ):
        """
        If the collection is not set in constructor, one can set it here.
        :param collection_map: dictionary of <str, numpy array>
        """
        self.lst_formula_id = list(range(1, 4329))
        temp = numpy.load("formula_embedding_library/fastText_models/slt_cards_math_deck.npy", allow_pickle=True)
        self.doc_tensors_slt = torch.FloatTensor(temp)
        temp = numpy.load("formula_embedding_library/fastText_models/slt_type_cards_math_deck.npy", allow_pickle=True)
        self.doc_tensors_slt_type = torch.FloatTensor(temp)
        temp = numpy.load("formula_embedding_library/fastText_models/opt_cards_math_deck.npy", allow_pickle=True)
        self.doc_tensors_opt = torch.FloatTensor(temp)

    def __latex_formula_to_vector_old(self, formula_latex):
        time_start = time.time()
        try:
            slt_tuples = Tuple_Extraction.latex_math_to_slt_tuples(formula_latex)
        except:
            slt_tuples = None
        try:
            opt_tuples = Tuple_Extraction.latex_math_to_opt_tuples(formula_latex)
        except:
            opt_tuples = None
        time_end = time.time()

        print("tuple extraction time: " + str(time_end - time_start) + " seconds")
        ######################################SLT
        time_start = time.time()
        if slt_tuples is None and opt_tuples is None:
            raise Exception("Extraction Error")
        if slt_tuples is not None:
            lst_encoded_slt = self.math_encoder.encode_lst_tuples(slt_tuples,
                                                                  embedding_type=TupleTokenizationMode.Both_Separated)
            lst_encoded_slt_type = self.math_encoder.encode_lst_tuples(slt_tuples,
                                                                       embedding_type=TupleTokenizationMode.Type)
            slt_vec = self.slt_model.get_query_vector(lst_encoded_slt)
            slt_type_vec = self.slt_type_model.get_query_vector(lst_encoded_slt_type)
        else:
            slt_vec = None
            slt_type_vec = None
        if opt_tuples is not None:
            lst_encoded_opt = self.math_encoder.encode_lst_tuples(opt_tuples,
                                                                  embedding_type=TupleTokenizationMode.Both_Separated)
            opt_vec = self.opt_model.get_query_vector(lst_encoded_opt)
        else:
            opt_vec = None
        time_end = time.time() - time_start

        print("vector extraction time: " + str(time_end) + " seconds")
        return slt_vec, slt_type_vec, opt_vec

    def __latex_formula_to_vector(self, formula_latex):
        # time_start = time.time()
        try:
            slt_tuples, opt_tuples = Tuple_Extraction.get_tuples(formula_latex)
        except:
            slt_tuples = None
            opt_tuples = None

        # time_end = time.time()
        #
        # print("tuple extraction time: " + str(time_end - time_start) + " seconds")
        ######################################SLT
        # time_start = time.time()
        if slt_tuples is None and opt_tuples is None:
            raise Exception("Extraction Error")
        if slt_tuples is not None:
            lst_encoded_slt = self.math_encoder.encode_lst_tuples(slt_tuples,
                                                                  embedding_type=TupleTokenizationMode.Both_Separated)
            lst_encoded_slt_type = self.math_encoder.encode_lst_tuples(slt_tuples,
                                                                       embedding_type=TupleTokenizationMode.Type)
            slt_vec = self.slt_model.get_query_vector(lst_encoded_slt)
            slt_type_vec = self.slt_type_model.get_query_vector(lst_encoded_slt_type)
        else:
            slt_vec = None
            slt_type_vec = None
        if opt_tuples is not None:
            lst_encoded_opt = self.math_encoder.encode_lst_tuples(opt_tuples,
                                                                  embedding_type=TupleTokenizationMode.Both_Separated)
            opt_vec = self.opt_model.get_query_vector(lst_encoded_opt)
        else:
            opt_vec = None
        # time_end = time.time() - time_start
        #
        # print("vector extraction time: " + str(time_end) + " seconds")
        return slt_vec, slt_type_vec, opt_vec

    def retrieval(self, formula_latex, limit=10):
        # time1 = time.time()
        slt_vec, slt_type_vec, opt_vec = self.__latex_formula_to_vector(formula_latex)
        # time2 = time.time()
        # print("get query vector time:" + str(time2 - time1))
        # time1 = time.time()
        if slt_vec is not None:
            query_vector_slt = torch.FloatTensor(slt_vec.reshape(300, 1))
            slt_result = Counter(self.__get_ret_result(query_vector_slt, self.doc_tensors_slt))
        else:
            slt_result = Counter({})
        if slt_type_vec is not None:
            query_vector_slt_type = torch.FloatTensor(slt_type_vec.reshape(300, 1))
            slt_type_result = Counter(self.__get_ret_result(query_vector_slt_type, self.doc_tensors_slt_type))
        else:
            slt_type_result = Counter({})
        if opt_vec is not None:
            query_vector_opt = torch.FloatTensor(opt_vec.reshape(300, 1))
            opt_result = Counter(self.__get_ret_result(query_vector_opt, self.doc_tensors_opt))
        else:
            opt_result = Counter({})
        # time2 = time.time()
        # print("All 3 systems: " + str(time2 - time1))

        # time1 = time.time()
        temp_dic = dict(slt_result + slt_type_result + opt_result)
        temp_dic = {k: v for k, v in sorted(temp_dic.items(), key=lambda item: item[1], reverse=True)}
        temp_dic = self.normalize(temp_dic)
        # time2 = time.time()
        # print("Combining results time: " + str(time2 - time1))
        # time1 = time.time()
        final_res = [(k, v) for k, v in temp_dic.items()][:limit]
        # time2 = time.time()
        # print("Returned results time: " + str(time2 - time1))
        return final_res
        # return dict(list(temp_dic.items())[0: limit])
        # return n_items#{k: temp_dic[k] for k in list(temp_dic)[:limit]}

    def __get_ret_result(self, query_vec, doc_tensors):
        retrieval_result = {}
        dist = (torch.matmul(doc_tensors, query_vec)).view(-1)
        sort_by_score = torch.sort(dist, descending=True)
        index_sorted = sort_by_score[1].data.cpu().numpy()

        # top_100 = index_sorted
        # cos_values = torch.sort(dist, descending=True)[0].data.cpu().numpy()
        # count = 1
        # for x in top_100:
        #     doc_id = self.lst_formula_id[x]
        #     score = cos_values[count - 1]
        #     retrieval_result[doc_id] = score / count
        #     count += 1
        # result = {k: v for k, v in sorted(retrieval_result.items(), key=lambda item: item[1], reverse=True)}
        # return result

        for i in range(0, len(index_sorted)):
            retrieval_result[self.lst_formula_id[index_sorted[i]]] = 1 / (i + 1 + 60)
        return retrieval_result

    def normalize(self, temp_dic):
        max_value = max(list(temp_dic.values()))
        min_value = min(list(temp_dic.values()))
        diff = 1
        if max_value != min_value:
            diff = max_value - min_value
        # for item in temp_dic:
        #     temp_dic[item] = (temp_dic[item]-min_value)/diff
        temp_dic.update((x, (y - min_value) / diff) for x, y in temp_dic.items())
        return temp_dic
