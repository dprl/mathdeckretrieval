import csv


def read_formula_file(csv_file_path):
    dic_formulas = {}
    file = open(csv_file_path, newline='', encoding="utf-8")
    csv_reader = csv.reader(file, delimiter=',', quotechar='"')
    for row in csv_reader:
        formula_id = row[0]
        formula_latex = row[1]
        dic_formulas[formula_id] = formula_latex
    return dic_formulas

def read_formula_file2(csv_file_path):
    dic_formulas = {}
    file = open(csv_file_path, newline='', encoding="utf-8")
    csv_reader = csv.reader(file, delimiter='\t', quotechar='"')
    for row in csv_reader:
        formula_id = row[0]
        formula_latex = row[1]
        dic_formulas[formula_id] = formula_latex
    return dic_formulas

def recreate_new_file():
    formulas_missed = read_formula_file2("formula_opts")
    with open("new_opts", "w", newline='', encoding="utf-8") as file:
        csv_writer = csv.writer(file, delimiter='\t', quotechar='"')
        for item in formulas_missed:
            opt = formulas_missed[item]
            opt = opt.replace("<share href=\"#Ex1.m1.sh1\"></share>", "", opt.count("<share href=\"#Ex1.m1.sh1\"></share>"))
            opt = opt.replace("<share href=\"#Ex1.m1.sh2\"></share>", "",
                              opt.count("<share href=\"#Ex1.m1.sh2\"></share>"))
            opt = opt.replace("<share href=\"#Ex1.m1.sh3\"></share>", "",
                              opt.count("<share href=\"#Ex1.m1.sh3\"></share>"))
            opt = opt.replace("<share href=\"#Ex1.m1.sh4\"></share>", "",
                              opt.count("<share href=\"#Ex1.m1.sh4\"></share>"))
            opt = opt.replace("<share href=\"#Ex1.m1.sh5\"></share>", "",
                              opt.count("<share href=\"#Ex1.m1.sh5\"></share>"))
            opt = opt.replace("<share href=\"#Ex1.m1.sh6\"></share>", "",
                              opt.count("<share href=\"#Ex1.m1.sh6\"></share>"))
            opt = opt.replace("<csymbol cd=\"latexml\">differential-d</csymbol>", "<ci>d</ci>",
                              opt.count("<csymbol cd=\"latexml\">differential-d</csymbol>"))
            csv_writer.writerow([item, opt])

recreate_new_file()
# formulas = read_formula_file("SQLite.csv")
# formulas_missed = read_formula_file2("formula_opts")
# lst_temp = []
# file = open("missed_opt", "w", encoding="utf-8")
# for key in formulas:
#     if key not in formulas_missed:
#         lst_temp.append(key)
#         file.writelines(key+"\n")
# file.close()
#
# print(len(lst_temp))