from .math_util.math_extractor import MathExtractor


def latex_math_to_slt_tuples(latex_formula):
    temp = MathExtractor.parse_from_tex(latex_formula)
    return temp.get_pairs(window=2, eob=True)


def latex_math_to_opt_tuples(latex_formula):
    temp = MathExtractor.parse_from_tex22(latex_formula)
    return temp.get_pairs(window=2, eob=True)


def latex_math_to_slt(latex_formula):
    return MathExtractor.parse_from_tex(latex_formula)


def latex_math_to_opt(latex_formula):
    return MathExtractor.parse_from_tex22(latex_formula)


def get_tuples(formula_latex):
    # print("get tuples method")
    slt, opt = MathExtractor.get_slt_opt(formula_latex)
    # print(slt)
    # print(opt)
    slt_tuples = slt.get_pairs(window=2, eob=True)
    opt_tuples = opt.get_pairs(window=2, eob=True)
    return slt_tuples, opt_tuples
