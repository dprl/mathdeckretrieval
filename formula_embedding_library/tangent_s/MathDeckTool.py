import csv
import os
import sys

os.chdir(sys.path[0])
from Tuple_Extraction import latex_math_to_slt
from Tuple_Extraction import latex_math_to_opt


def read_formula_file(csv_file_path):
    dic_formulas = {}
    file = open(csv_file_path, newline='')
    csv_reader = csv.reader(file, delimiter=',', quotechar='"')
    for row in csv_reader:
        formula_id = row[0]
        formula_latex = row[1]
        dic_formulas[formula_id] = formula_latex
    return dic_formulas


def try_slt(formula_dict_latex):
    count_exception = 0
    count_exception_dollar = 0
    map_slt_tuple = {}
    for formula_id in formula_dict_latex:
        try:
            latex_formula = formula_dict_latex[formula_id]
            slt = latex_math_to_slt(latex_formula)
            print(slt)
            map_slt_tuple[formula_id] = slt
        except:
            print("failed on conversion: " + latex_formula)
            count_exception += 1
            try:
                slt = latex_math_to_slt("$"+latex_formula+"$")
                map_slt_tuple[formula_id] = slt
            except:
                print("failed on conversion again: " + latex_formula)
                count_exception_dollar += 1
                pass
    return map_slt_tuple, count_exception, count_exception_dollar


def try_opt(formula_dict_latex):
    count_exception = 0
    count_exception_dollar = 0
    map_opt_tuple = {}
    for formula_id in formula_dict_latex:
        # try:
        latex_formula = formula_dict_latex[formula_id]
        opt = latex_math_to_opt(latex_formula)
        map_opt_tuple[formula_id] = opt
        # except:
        #     print("failed on conversion: " + latex_formula)
        #     count_exception += 1
        #     try:
        #         opt = latex_math_to_opt("$"+latex_formula+"$")
        #         map_opt_tuple[formula_id] = opt
        #     except:
        #         print("failed on conversion again: " + latex_formula)
        #         count_exception_dollar += 1
        #         pass
    return map_opt_tuple, count_exception, count_exception_dollar

def try_opt_missed(formula_dict_latex):
    count_exception = 0
    count_exception_dollar = 0
    map_opt_tuple = {}
    with open('formula_opts_missed', 'w', newline='', encoding="utf-8") as outfile:
        writer = csv.writer(outfile, delimiter='\t', quotechar='"')
        for formula_id in formula_dict_latex:
            latex_formula = formula_dict_latex[formula_id]
            opt = latex_math_to_opt(latex_formula)
            print(formula_id+"\t")
            print(opt)
            writer.writerow([formula_id, opt])
            map_opt_tuple[formula_id] = opt
    return map_opt_tuple, count_exception, count_exception_dollar

map_formula_lst = read_formula_file("SQLite.csv")
# print("Number of formulas %d" % (len(map_formula_lst.keys())))

# map_slt_tuple, count_exception, count_exception_dollar = try_slt(map_formula_lst)
# print(count_exception / len(map_formula_lst.keys()))
# print(count_exception_dollar / len(map_formula_lst.keys()))
# with open('formula_slts2', 'w', newline='', encoding="utf-8") as outfile:
#     writer = csv.writer(outfile, delimiter='\t', quotechar='"')
#     for formula_id in map_slt_tuple:
#         writer.writerow([formula_id, map_slt_tuple[formula_id]])
# print("-------------------------------------")
# map_opt_tuple, count_exception, count_exception_dollar = try_opt(map_formula_lst)
# print(count_exception / len(map_formula_lst.keys()))
# print(count_exception_dollar / len(map_formula_lst.keys()))
# with open('formula_opts_missed', 'w', newline='', encoding="utf-8") as outfile:
#     writer = csv.writer(outfile, delimiter='\t', quotechar='"')
#     for formula_id in map_opt_tuple:
#         writer.writerow([formula_id, map_opt_tuple[formula_id]])

def read_missed(file_path):
    dict_formula_ids = {}
    file = open(file_path, encoding="utf-8")
    line = file.readline()
    while line:
        formula_id = line.strip()
        latex = map_formula_lst[formula_id]
        dict_formula_ids[formula_id] = latex
        line = file.readline()
    file.close()
    return dict_formula_ids


missed_opts_dict = read_missed("missed_opt")
print("-------------------------------------")
map_opt_tuple, count_exception, count_exception_dollar = try_opt_missed(missed_opts_dict)
print(count_exception / len(missed_opts_dict.keys()))
print(count_exception_dollar / len(missed_opts_dict.keys()))
