from .embedding_preprocessing.tuple_encoder import TupleEncoder


class MathDeckEncoder:
    def __init__(self, encoder_map_node={}, encoder_map_edge={}):
        self.encoder_map_node = encoder_map_node
        self.encoder_map_edge = encoder_map_edge
        if len(self.encoder_map_edge.keys()) > 0:
            self.node_id = max(list(self.encoder_map_node.values())) + 1
            self.edge_id = max(list(self.encoder_map_edge.values())) + 1
        else:
            self.node_id = 60000
            self.edge_id = 500

    def encode_lst_tuples(self, list_of_tuples, embedding_type):
        encoded_tuples, update_map_node, update_map_edge, node_id, edge_id = \
            TupleEncoder.encode_tuples(self.encoder_map_node, self.encoder_map_edge, self.node_id, self.edge_id,
                                       list_of_tuples, embedding_type, ignore_full_relative_path=True,
                                       tokenize_all=False,
                                       tokenize_number=False)
        self.node_id = node_id
        self.edge_id = edge_id
        self.encoder_map_node.update(update_map_node)
        self.encoder_map_edge.update(update_map_edge)
        return encoded_tuples

    def load_encoder_map(self, map_file_path):
        file = open(map_file_path)
        line = file.readline().strip("\n")
        while line:
            parts = line.split("\t")
            encoder_type = parts[0]
            symbol = parts[1]
            value = int(parts[2])
            if encoder_type == "N":
                self.encoder_map_node[symbol] = value
            else:
                self.encoder_map_edge[symbol] = value
            line = file.readline().strip("\n")
        self.node_id = max(list(self.encoder_map_node.values())) + 1
        self.edge_id = max(list(self.encoder_map_edge.values())) + 1
        print(self.node_id)
        print(self.edge_id)
        file.close()
