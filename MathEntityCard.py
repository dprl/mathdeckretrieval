from formula_embedding_library import FormulaEmbedding as FE, MathExtractor as ME
import sqlite3
from util import symbol_value as sv
import numpy as np
from marshmallow import Schema, fields
from flask import jsonify
import time
import string

class MathEntityCard:
    symbol_value_dict = None
    formula_embedding = None

    def __init__(self):
        '''
            Check if symbol encodings exists in DB 
            if yes, read from DB since it has updated copy
            If not read from file, store to DB and persists any updates to DB as well
        '''
        self.conn = sqlite3.connect('entity_card.db',check_same_thread=False)
        self.symbol_value_dict = sv.get_encoder_dict(self.conn)
        if(not self.symbol_value_dict):
            # DB is empty read from file.
            self.symbol_value_dict = sv.read_symbol_value_from_file()
            # Persist to DB
            sv.save_encoder_dict(self.symbol_value_dict,self.conn)

        
        # all_formulaID_embedding = self.get_all_formula_embedding()
        # self.formula_embedding = FE(all_formulaID_embedding)
        self.formula_embedding = FE("formula_embedding_library/fastText_cft_models/slt_102",
                                    "formula_embedding_library/fastText_cft_models/opt_100",
                                    "formula_embedding_library/fastText_cft_models/slt_type_104")

    def get_all_formula_embedding(self):
        curr = self.conn.cursor()
        curr.execute("Select FORMULA_ID,FORMULA_EMBED FROM FORMULAS ")
        rows = curr.fetchall()
        all_formula_embedding = {}

        for row in rows:
            formula_id = row[0]
            formula_embedding = np.fromstring(row[1])
            all_formula_embedding[formula_id] = formula_embedding

        return all_formula_embedding

    def get_card_data(self, formulaID_score):
        match_formula_ID = formulaID_score[0]
        confidence = float(formulaID_score[1])*100

        curr = self.conn.cursor()
        curr.execute("SELECT C.concept_id, C.concept_name FROM   concepts AS C"
                " INNER JOIN concept_formula_mapping AS CFM"
                " ON CFM.concept_id = C.concept_id "
                " WHERE  CFM.formula_id =:id",{'id':match_formula_ID})
        concept_result = curr.fetchone()
        concept_id = concept_result[0]
        concept_name = concept_result[1]

        curr.execute("SELECT F.formula_id, F.formula_latex,F.formula_mathml,"
                " F.formula_symbols_json "
                " FROM   concept_formula_mapping CFM "
                " INNER JOIN formulas AS F ON CFM.formula_id = F.formula_id"
                " WHERE  CFM.concept_id = :id",{'id':concept_id})
        formula_results = curr.fetchall()

        all_formula_concept = []
        if formula_results:
            for ind_formula in formula_results:
                formula_obj = Formula(id=ind_formula[0],latex=ind_formula[1],
                math_ml=ind_formula[2],symbols=ind_formula[3]
                )
                all_formula_concept.append(formula_obj)


        # curr.execute("SELECT D.desc_id, D.description, D.source_url"
        #         " FROM   concept_description_mapping CDM"
        #         " INNER JOIN descriptions AS D ON CDM.desc_id = D.desc_id "
        #         " WHERE CDM.concept_id = :id",{'id':concept_id})

        curr.execute("SELECT D.desc_id, D.description, D.source_url, DS.SOURCE_NAME"
                " FROM   concept_description_mapping CDM"
                " INNER JOIN descriptions AS D ON CDM.desc_id = D.desc_id "
                " INNER JOIN DATA_SOURCES DS on D.DATA_SOURCE_ID = DS.DS_ID "
                " WHERE CDM.concept_id = :id ORDER BY DS.DS_ID",{'id':concept_id})
        desc_results = curr.fetchall()

        all_desc_concept = []

        if desc_results:
            for ind_desc in desc_results:
                desc_obj = Description(id = ind_desc[0], description=ind_desc[1],
                source_link=ind_desc[2],
                source_name=ind_desc[3])
                all_desc_concept.append(desc_obj)
        else:
            desc = Description(id='0',description='No Description Found',
            source_name="None",
        source_link="_blank")

        ind_entity_card = EntityCard(concept_id, string.capwords(concept_name),
                            all_formula_concept,confidence,all_desc_concept)
        return ind_entity_card

    def get_latex_embedding(self, formula_mml):
        formula_embedding, new_sym_enco_dict = self.formula_embedding.mml_formula_to_vector(self.symbol_value_dict, formula_mml)
        if len(new_sym_enco_dict.keys()):
            self.symbol_value_dict.update(new_sym_enco_dict)
            sv.save_encoder_dict(new_sym_enco_dict,self.conn)
        return formula_embedding

    def get_formulaIDs_from_embedding(self,formula_embedding):
        result = self.formula_embedding.retrieval(formula_embedding)
        return result

    def get_entity_card(self, formula_ltx):
        start = time.time()
        # formula_mml = ME.pandoc_tex_mml(formula_ltx)
        print(" Time to Latex TO MML : ", time.time()-start)
        return self.get_entity_card_latex(formula_ltx)

    def get_entity_card_latex(self, formula_ltx):
        start = time.time()
        all_fID_scores = self.formula_embedding.retrieval(formula_ltx)
        #self.get_formulaIDs_from_embedding(formula_embedding)
        print(" Time to Retrieve  : ",time.time()-start)
        schema = EntityCardSchema(many=True)

        result = None

        all_entity_cards = []
        start = time.time()
        for formulaID_score in all_fID_scores:
            ind_entity_card = self.get_card_data(formulaID_score)
            if ind_entity_card:
                all_entity_cards.append(ind_entity_card)
        print(" Time to get EC Card Data : ",time.time()-start)
        result = schema.dump(all_entity_cards)

        if result:
            return jsonify(result.data)
        else:
            return jsonify([]),404

    def get_entity_card_mml(self, formula_mml):
        start = time.time()
        formula_embedding = self.get_latex_embedding(formula_mml)
        print(" Time to embed  : ",time.time()-start)
        start = time.time()
        all_fID_scores = self.get_formulaIDs_from_embedding(formula_embedding)
        print(" Time to Retrieve  : ",time.time()-start)
        schema = EntityCardSchema(many=True)

        result = None

        all_entity_cards = []
        start = time.time()
        for formulaID_score in all_fID_scores:

            ind_entity_card = self.get_card_data(formulaID_score)
            if ind_entity_card:
                all_entity_cards.append(ind_entity_card)
        print(" Time to get EC Card Data : ",time.time()-start)
        result = schema.dump(all_entity_cards)

        if result:
            return jsonify(result.data)
        else:
            return jsonify([]),404
    
    def get_mathml_length(self):
        curr = self.conn.cursor()
        curr.execute("SELECT F.formula_id,  F.FORMULA_LATEX"
                " FROM   FORMULAS  F ")
        formula_results = curr.fetchall()

        mml_size = []
        for formula  in formula_results:
            formula_id = formula[0]
            formula_latex = formula[1]
            formula_mml = ME.pandoc_tex_mml(f'${formula_latex}$')
            
            size = self.formula_embedding.get_mathml_size(formula_mml)
            mml_size.append( (formula_id,size) )
        

        curr.executemany('INSERT INTO FORMULA_MML_SIZE VALUES (?,?)',mml_size)
        self.conn.commit()
    
    def get_conceptIDs_for_textautocomplete(self,text_query):
        """
            Queries the DB and orders by concept name
            The order by is important as it combined with
            the dict which is an ordered dict by default
            helps break ties later on in sorting.
        """

        match_any_query = "%"+text_query+"%"
        curr = self.conn.cursor()
        curr.execute("SELECT C.concept_id, C.concept_name  FROM concepts AS C "
        " WHERE  Lower(C.concept_name) LIKE :tq "
        " ORDER  BY C.concept_name ",{'tq':match_any_query})

        #This is the query for both Concept and Alias
        # curr.execute("SELECT C.concept_id, C.concept_name  FROM concepts AS C "
        # " WHERE  C.concept_id IN (SELECT C.concept_id "
        # " FROM   concepts AS C "
        # " WHERE  Lower(C.concept_name) LIKE :tq "
        # " UNION "
        # " SELECT CAM.concept_id "
        # " FROM   aliases AS A "
        # "        INNER JOIN concept_alias_mapping CAM "
        # "                ON CAM.alias_id = A.alias_id "
        # " WHERE  Lower(A.alias) LIKE :tq ) "
        # " ORDER  BY C.concept_name ",{'tq':match_any_query})
        rows = curr.fetchall()
        dict_match_conceptName_to_conceptID = {}
        dict_match_conceptID_to_conceptName = {}
        for row in rows:
            conceptName = row[1].lower()
            conceptID = row[0]
            dict_match_conceptName_to_conceptID[conceptName] = conceptID
            dict_match_conceptID_to_conceptName[conceptID] = conceptName

        return dict_match_conceptName_to_conceptID, dict_match_conceptID_to_conceptName

    def rank_concept_name_by_match_pos(self,text_query, all_match_conceptName_ID, match_conceptID_name, limit=10):
        # The intermediate result holds the first sort based on the 
        # index of the match, this is needed since there could be clash between matches
        intermed_result = {}
        for i in range(limit):
            intermed_result[i]=[]
        
        total_results = 0 
        # Greater than limit is a dict acts as an overflowbuffer
        # key is positionmatched, the value is the concept_name
        greater_than_limit = {}

        for concept_name in all_match_conceptName_ID.keys():
            pos_match = concept_name.index(text_query)
            if pos_match < limit: 
                intermed_result[pos_match].append(all_match_conceptName_ID.get(concept_name))
                total_results += 1
            else:
                if pos_match not in greater_than_limit:
                    greater_than_limit[pos_match] = [concept_name]
                else:
                    greater_than_limit[pos_match].append(concept_name)

        
        #Create the final result 
        final_result_conceptID = []

        for i in range(limit):
            ranked_conceptID = intermed_result.get(i)
            size_ranked_conceptID = len(ranked_conceptID)
    
            if size_ranked_conceptID < len(final_result_conceptID):
                final_result_conceptID.extend(ranked_conceptID)
            else:
                balance = limit - size_ranked_conceptID
                final_result_conceptID.extend(ranked_conceptID[:balance])

            if len(final_result_conceptID) > limit:
                break
        
        # There could be some matches that are not in the top limit position 
        # these would be in the greater than dictionary, Append by order.
        if len(final_result_conceptID) < limit and len(greater_than_limit.keys()) > 1:
            for key in sorted (greater_than_limit.keys()):
                ranked_greated_conceptID = greater_than_limit.get(key)
                size_ranked_conceptID = len(ranked_greated_conceptID)
                balance = limit - size_ranked_conceptID
                final_result_conceptID.extend(ranked_conceptID[:balance])

                if len(final_result_conceptID) > limit:
                    break
        
        #Reduce results to the limit
        if len(final_result_conceptID) > limit:
            final_result_conceptID = final_result_conceptID[:limit]

        return final_result_conceptID    

    
    # def order_matched_conceptID(self,text_query,dict_match_conceptID_to_conceptName, top_matched_concept):
    ## This is to re-order by the percentage of overlap unfortunately it conflicts 
    ## with the offset based result hence commented.

    #     intermediate_result = {}
    #     for conceptID in top_matched_concept:
    #         concept_name = dict_match_conceptID_to_conceptName.get(conceptID)
    #         confidence = float(len(text_query)/len(concept_name))*100
    #         print(confidence)
    #         intermediate_result[confidence] = conceptID
        
    #     final_result = []
    #     for key in sorted(intermediate_result.keys(),reverse=True):
    #         final_result.append(intermediate_result.get(key))
        
    #     return final_result


    def get_entity_card_title_alias(self, text_query):
        #Similar to a full text search but ranked based on offset of match from the left
        # like a prefix ranked. We use full text search so as not to miss results
        # that contain query, which is possible with autocomplete

        dict_match_conceptName_to_conceptID, dict_match_conceptID_to_conceptName = \
        self.get_conceptIDs_for_textautocomplete(text_query)

        top_matched_concept = self.rank_concept_name_by_match_pos(text_query,dict_match_conceptName_to_conceptID,dict_match_conceptID_to_conceptName)
        
        # Ranking based on confidence is commented as it 
        # Conflicts with the offset method.
        # ranked_matched_concept = self.order_matched_conceptID(text_query, dict_match_conceptID_to_conceptName,top_matched_concept)
        schema = EntityCardSchema(many=True)
        result = None
        all_entity_cards = []
        # start = time.time()
        for conceptID in top_matched_concept:
            ind_entity_card = self.get_card_data_for_conceptID(conceptID,text_query)
            if ind_entity_card:
                all_entity_cards.append(ind_entity_card)
        # print(" Time to get EC Card Data Text Query: ",time.time()-start)
        result = schema.dump(all_entity_cards)

        if result:
            return jsonify(result.data)
        else:
            return jsonify([]),404
    
    def get_card_data_for_conceptID(self,conceptID, text_query):

        curr = self.conn.cursor()
        curr.execute("SELECT C.concept_id, C.concept_name FROM   concepts AS C"
                " INNER JOIN concept_formula_mapping AS CFM"
                " ON CFM.concept_id = C.concept_id "
                " WHERE  CFM.concept_id =:id",{'id':conceptID})
        concept_result = curr.fetchone()
        concept_id = concept_result[0]
        concept_name = concept_result[1]
        confidence = float(len(text_query)/len(concept_name))*100

        curr.execute("SELECT F.formula_id, F.formula_latex,F.formula_mathml,"
                " F.formula_symbols_json "
                " FROM   concept_formula_mapping CFM "
                " INNER JOIN formulas AS F ON CFM.formula_id = F.formula_id"
                " WHERE  CFM.concept_id = :id",{'id':concept_id})
        formula_results = curr.fetchall()

        all_formula_concept = []
        if formula_results:
            for ind_formula in formula_results:
                formula_obj = Formula(id=ind_formula[0],latex=ind_formula[1],
                math_ml=ind_formula[2],symbols=ind_formula[3]
                )
                all_formula_concept.append(formula_obj)


        # curr.execute("SELECT D.desc_id, D.description, D.source_url"
        #         " FROM   concept_description_mapping CDM"
        #         " INNER JOIN descriptions AS D ON CDM.desc_id = D.desc_id "
        #         " WHERE CDM.concept_id = :id",{'id':concept_id})

        curr.execute("SELECT D.desc_id, D.description, D.source_url, DS.SOURCE_NAME"
                " FROM   concept_description_mapping CDM"
                " INNER JOIN descriptions AS D ON CDM.desc_id = D.desc_id "
                " INNER JOIN DATA_SOURCES DS on D.DATA_SOURCE_ID = DS.DS_ID "
                " WHERE CDM.concept_id = :id ORDER BY DS.DS_ID",{'id':concept_id})
        desc_results = curr.fetchall()

        all_desc_concept = []

        if desc_results:
            for ind_desc in desc_results:
                desc_obj = Description(id = ind_desc[0], description=ind_desc[1],
                source_link=ind_desc[2],
                source_name=ind_desc[3])
                all_desc_concept.append(desc_obj)
        else:
            desc = Description(id='0',description='No Description Found',
            source_name="None",
        source_link="_blank")

        ind_entity_card = EntityCard(concept_id, string.capwords(concept_name),
                            all_formula_concept,confidence,all_desc_concept)
        return ind_entity_card


class Formula:

    def __init__(self,id = None, math_ml = None, latex=None,embedding=None,symbols=None, source=None):
        self.id = id
        self.math_ml = math_ml
        self.latex = latex
        self.embedding = embedding
        self.symbols = symbols
        self.source = source

    def __repr__(self):
        return " Formula ('{}','{}')".format(self.latex,self.symbols)

class FormulaSchema(Schema):
    id = fields.Integer()
    math_ml = fields.String()
    latex = fields.String()
    symbols = fields.String()

class Description:

    def __init__(self, description, source_name, id= None, source_link=None):
        self.id = id
        self.description = description
        self.source_name = source_name
        self.source_link = source_link

    def __repr__(self):
        return " Description('{}', taken from '{}  {}')".format(self.description,self.source_name,self.source_link)

class DescriptionSchema(Schema):
    id = fields.Integer()
    description = fields.String()
    source_name = fields.String()
    source_link = fields.String()

class EntityCard:

    def __init__(self,id , title, formula,confidence, descriptions=None):
        self.id = id
        self.title = title
        self.formula = formula
        self.confidence = confidence
        if(descriptions == None):
            self.descriptions = []
        else:
            self.descriptions = descriptions

    def addDescriptions(self,description):
        self.descriptions.append(description)

    def __repr__(self):
        return " EntityCard('{}','{}','{}')".format(self.title,self.formula,self.descriptions)

class EntityCardSchema(Schema):
    id = fields.Integer()
    title = fields.String()
    confidence = fields.String()
    formula = fields.Nested(FormulaSchema, only=['id','latex','symbols'],many=True)
    descriptions = fields.Nested(DescriptionSchema,many=True)

class Concept:

    def __init__(self, title, formulas=None, descriptions=None):
        self.title = title

        if(formulas == None):
            self.formulas = []
        else:
            self.formulas = formulas

        if(descriptions == None):
            self.descriptions = []
        else:
            self.descriptions = descriptions

    def addDescriptions(self,description):
        self.descriptions.append(description)

    def addFormula(self,formula):
        self.formulas.append(formula)

    def __repr__(self):
        return " EntityCard('{}','{}','{}')".format(self.title,self.formulas,self.descriptions)
