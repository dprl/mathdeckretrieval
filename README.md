# Math Entity Card Creation and API 

Extraction of titles, formula and description from Wikidata dump (JSON), Wiktionary (XML) and Proof WIki (XML) into a SQLite Database. 
A REST endpoint that takes an input LaTeX or MathML query and returns a JSON response of Title, Formula in LaTeX, Formula SVG for fast loading and multiple descriptions. 

## Getting Started

### Data
**Wikidata** : 

	Can be queried by its SPARQL end point and response can be saved to a JSON response.

Steps :

1. Go to [Wikidata SPARQL](https://query.wikidata.org/)
Enter the following SPARQL Query  (Without quotes): 

		"""SELECT ?item ?itemLabel ?formula ?itemDescription ?itemAlso WHERE {
		?item wdt:P2534 ?formula.
		FILTER(CONTAINS(STR(?formula), '</math>'))
		SERVICE wikibase:label
		{ bd:serviceParam wikibase:language "en". }
		}
		"""
2. Download JSON file and save it as 'wiki_data.json'  in extraction/data.

**Wikipedia Symbols**: Mathematical Symbols are available as a table on the [List of Mathematical Symbol Page](https://en.wikipedia.org/wiki/List_of_mathematical_symbols)

With the help of a modified verison of Wiki-table Scrape the mathematical symbols are extracted as CSV. 

Modifications of the code include:

		1. Changes to handle sub rows.
		2. Inclusion of a delimiter '<_>' to split data later.


**Wiktionary**: 

Data is available as XML dumps at [Wikimedia Dumps](https://dumps.wikimedia.org/backup-index.html).

Steps :

1.  We currently process only english Wiktionary dumps, navigate and download the latest dumps  e.g. 'enwiktionary-20190620-pages-articles-multistream.xml.bz2'.
2. Use the tar command to uncompress the file "tar xjf file.tar.bz2"
3. Place the extracted XML file e.g. 'enwiktionary-20190620-pages-articles-multistream.xml' in extraction/data.
4. Run the Wiktionary Parser.py as python wiktionary_parser.py  from within the extraction folder.
5. This creates a folder 'wikt_math' and places all the HTML files that have some math element, as decided using grep.
	 (Note although grep picks up multiple elements having math, some pages lose their math on converting to HTML using Pandoc (pypandoc) reasons not investigated)

**Proof Wiki**: 

Data is available as XML dumps at [Proof Wiki](https://proofwiki.org/xmldump/latest.xml) which needs to be cURL into a file
If the link changes navigate to the [questions page](https://proofwiki.org/wiki/Help:Questions#Is_it_possible_to_download_your_definitions_as_a_data_file.3F) and check for an update.

Steps:

1. Run Command:  curl https://proofwiki.org/xmldump/latest.xml --output proof_wiki.xml

2. Save the file with the exact name proof_wiki.xml under extraction/data

### Prerequisites

All necessary dependencies are present in the requirements.txt file.
It is recommend to install all packages within a [Python Virtual Environment](https://docs.python.org/3/tutorial/venv.html)

### Installing

#### Database Setup
		A clean install would require a new SQLite database, A tool such as [SQLite Browser](https://sqlitebrowser.org/) would be helpful.

		Create a new database named 'entity_card.db'.

		Run the following queries to setup the schema.

		1. CREATE TABLE `ALIASES` ( `ALIAS_ID` INTEGER NOT NULL, `ALIAS` TEXT, PRIMARY KEY(`ALIAS_ID`) )
		2. CREATE TABLE "CONCEPTS" ( `CONCEPT_ID` INTEGER NOT NULL, `CONCEPT_NAME` TEXT NOT NULL, `DATA_SOURCE_ID` INTEGER, PRIMARY KEY(`CONCEPT_ID`) )
		3. CREATE TABLE `CONCEPT_ALIAS_MAPPING` ( `CONCEPT_ID` INTEGER, `ALIAS_ID` INTEGER )
		4. CREATE TABLE `CONCEPT_DESCRIPTION_MAPPING` ( `CONCEPT_ID` INTEGER, `DESC_ID` INTEGER )
		5. CREATE TABLE `CONCEPT_FORMULA_MAPPING` ( `CONCEPT_ID` INTEGER, `FORMULA_ID` INTEGER )
		6. CREATE TABLE `CONCEPT_TAGS_MAPPING` ( `CONCEPT_ID` INTEGER, `TAG_ID` INTEGER )
		7. CREATE TABLE `DATA_SOURCES` ( `DS_ID` INTEGER NOT NULL, `SOURCE_NAME` TEXT, PRIMARY KEY(`DS_ID`) )
		8. INSERT INTO DATA_SOURCES (SOURCE_NAME) VALUES ('Wiktionary')
		9. INSERT INTO DATA_SOURCES (SOURCE_NAME) VALUES ('Wikipedia')
		10. INSERT INTO DATA_SOURCES (SOURCE_NAME) VALUES ('Wikidata')
		11. INSERT INTO DATA_SOURCES (SOURCE_NAME) VALUES ('Proof Wiki')
		12. CREATE TABLE "DESCRIPTIONS" ( `DESC_ID` INTEGER NOT NULL, `DESCRIPTION` TEXT, `SOURCE_URL` TEXT, `DATA_SOURCE_ID` INTEGER, PRIMARY KEY(`DESC_ID`) )
		13. CREATE TABLE "FORMULAS" ( `FORMULA_ID` INTEGER NOT NULL, `FORMULA_LATEX` TEXT, `FORMULA_EMBED` BLOB, `FORMULA_MATHML` TEXT, `FORMULA_SYMBOLS_JSON` TEXT, `DATA_SOURCE_ID` INTEGER, PRIMARY KEY(`FORMULA_ID`) )
		14. CREATE TABLE `FORMULA_MML_SIZE` ( `FORMULA_ID` INTEGER, `SIZE` INTEGER )
		15. CREATE TABLE `SYMBOL_VALUE_MAP` ( `SYMBOL` TEXT, `VALUE` INTEGER )
		16. CREATE TABLE `TAGS` ( `TAG_ID` INTEGER NOT NULL, `TAG_NAME` TEXT, PRIMARY KEY(`TAG_ID`) ) 


#### Extraction Process
To populate the database, go through the following steps

Steps:

1. Activate the python virtual environment with all dependencies installed.
2. Run the command 'python extraction/extractor.py' from  the summary card folder.

	This  will extract data from all the four sources in the following order
	* Wikidata
	* Wikipedia Symbols
	* Wiktionary HTML Files (Created after running wiktionary_parser.py as above)
	* Proof Wiki 

	One can comment out individual sections and have the database with only each section of the data loaded for analysis.

3. Any errors are written to the corresponding file prefixed with pandoc_error_*(Source_Name).txt and placed within the extraction folder.

		e.g. pandoc_error_wiktionary.txt
4. Wiktionary requires two passes of the data and hence writes a file. 'second_pass.txt' again within the extraction folder

##### Generation of SVG Symbols:
Generation of SVG symbols is by a script within MathSeer Front End and will be released when the code is released.

A simple alternative is to load all the cards, and save the SVG rendered by MathJAX to a JSON file 

The SVGs need to have the Formula ID to enable the right mapping.

Make use of the scripts within Scripts folder  (Check Additional Scripts Section Below)
#### Starting the Service

The service can be started by running the following command within the activated virtual environment.

python MECAPI.py

The server should start up on http://127.0.0.1:5000/ unless already occupied.

#### Querying the API

The API can be queried using the command line or a tool such as [Insomnia](https://insomnia.rest/)

Request URL : http://127.0.0.1:5000/entityCards

		Request Type : POST

		Header: Content-Type application/json

		Request Params: { "formula":"a^2+b^2=c^2"}
		or
		Request Params: { "formulaMathML" : "Corresponding MathML"}


Response includes an array of Entity Card Data each containing.

1. Confidende Score a Percentage between 1 and 100 for the formula
2.  Title: Mathematical Concept Name
		id: Concept_ID as in the DB
3. A list of description objects each containing.
	* 	description : Description text possibly containing math that needs to be rendered via MathJax
	* 	id : Description ID
	* 	source_link: The source from where the description is fetched
	*	source_name: Source Name to be displayed on the card.
4. Formula Object
	* 	id: Formula ID
	* 	latex: LaTeX representation of the formula 
	*	symbols: Prerendered SVG Symbols to reduce render time.


A sample response is as follows:

  [{
    "confidence": "95.22329568862915",
    "descriptions": [
      {
        "description": "In mathematics, the  Pythagorean theorem , also known as  Pythagoras' theorem , is a fundamental relation in Euclidean geometry among the three sides of a right triangle. It states that the area of the square whose side is the hypotenuse (the side opposite the right angle) is equal to the sum of the areas of the squares on the other two sides.",
        "id": 4024,
        "source_link": "https://en.wikipedia.org/wiki/Pythagorean_theorem",
        "source_name": "Wikipedia"
      }
    ],
    "formula": [
      {
        "id": 3246,
        "latex": "c^{2} = a^{2} + b^{2}",
        "symbols": null
      }
    ],
    "id": 3109,
    "title": "Pythagorean Theorem"
  }
  ]
#### Additional Scripts 

There are two additional scripts.

1.	add_formula_svg.py : This takes a JSON file  of SVGs that has pre-rendered formula symbols and ID and updates the database with the enteries.
2. 	update_embedding.py: If Tangent-CFT model to embed formula is updated, this script can be used to updated all the embeddings.
	(Note: The server needs a down time to prevent dirty reads between old formula embedding vectors and new ones.)

## Deployment
Note this applications makes use of Flask's built in lightweight server is not designed to scale and is not suitable for production.

Please check for alternatives [here](https://flask.palletsprojects.com/en/1.1.x/deploying/)

## Future Work:

* Tags and Aliases are stored in the Database but are not yet a part of the response.

## Built With

* [Python 3](https://www.python.org/download/releases/3.0/)
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) 
* [Pypandoc](https://pypi.org/project/pypandoc/)
* [Wikipedia-Api](https://pypi.org/project/Wikipedia-API/)

## Authors

* **Abishai Dmello** - *Initial work* 

## License

This project is licensed under the GNU GPLv3  License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Andy Roche for the code on [Scraping Wikipedia Tables with Python](https://github.com/rocheio/wiki-table-scrape)  which has been modified to handled extraction of Symbols.
* Behrooz Mansouri for Tangent-CFT  'Tangent-CFT: An Embedding Model for Mathematical Formulas .'
* Gavin Nishizawa for code improvements and the script storing SVGs to enable faster load.
*  Richard Zanibbi for suggestions on parsing and handling data.

