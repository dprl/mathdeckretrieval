from . import data_access as da

def read_symbol_value_from_file():
    '''
        Runs the very first time the application is started any other restart would read the data from the DB
    '''
    sym_enco_dict = {}
    with open('formula_embedding_library/embedding_preprocessing/encoder_map.csv','r') as encoder_file:
        for line in encoder_file:
            content = line.rstrip().split(',')
            sym_enco_dict[content[0]] = int(content[1])
    return sym_enco_dict

def save_encoder_dict(sym_enco_dict,conn):
    '''
     sym_enco_dict is a dictionary
    '''
    data = []
    for symbol in sym_enco_dict:
        data.append((symbol, sym_enco_dict[symbol]))
    da.save_encoder_dict(data,conn)

def get_encoder_dict(conn):

    encoder_map_dict = da.load_encoder_dict(conn)

    return encoder_map_dict
