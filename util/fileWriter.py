import csv
import pypandoc

def main():

    baseDir = "/allformulas/"
    dicForm = {}
    with open('wikiDataFormula_ID.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\t')
        next(csv_reader)
        for row in csv_reader:
            dicForm[row[0]] = row[1]


    for key in dicForm.keys():
        output = pypandoc.convert_text(dicForm[key], 'latex', format='html')
        fileName = key+".html"
        with open(fileName,"w") as mathFile:
            mathFile.write(output)

if __name__ == '__main__':
    main()
