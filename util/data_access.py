import sqlite3
from sqlite3 import Error


def save_encoder_dict(data,conn):
    save_status =  False
    with conn:
        cur = conn.cursor()
        try:
            cur.executemany('INSERT INTO SYMBOL_VALUE_MAP VALUES (?,?)', data)
            conn.commit()
            save_status = True
        except sqlite3.Error as e:
            print("An error occurred, In saving updated symbol value map to Database : ", e.args[0])
            print('\n\n\n')
          # print('Database Error:  In saving updated symbol value map to Database.\n\n\n')
            conn.rollback()
        except :
            print('UNHANDLED EXCEPTION:  In saving updated symbol value map to DB.')
            conn.rollback()
        return save_status

def load_encoder_dict(conn):
    """
    Query all rows in the SYMBOL_VALUE_MAP table
    :param conn: the Connection object
    :return:
    """

    encoder_map_value = {}
    with conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM SYMBOL_VALUE_MAP")

        rows = cur.fetchall()

        for row in rows:
            encoder_map_value[row[0]] = row[1]

    return encoder_map_value
